# playingcards.io

GAme files for the playingcards.io website

The site https://playingcards.io allows users to create game rooms
that can be used on the internet.

Users may clone predefined games, or create their own.

This repository contains room definitions for various games.

